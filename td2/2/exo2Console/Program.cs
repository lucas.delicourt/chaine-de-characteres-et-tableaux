﻿using System;

namespace exo2Console
{
    class Program
    {
        public static void Main(string[] args)
        {
            int[] tab = new int[10];
            
            tab = initialisationTab();

            comparaison(tab);
            
            
        }

        public static void comparaison(int[] tab){
            Console.WriteLine("Saisissez une valeur");
            
            int chiffre = int.Parse(Console.ReadLine());

            for(int i=0;i<10;i++)
            {
                if(chiffre == tab[i])
                {
                    Console.WriteLine("{0} est la {1}-ème valeur", chiffre, i+1);
                }
            }
        }
        public static int[] initialisationTab(){
            int[] tab = new int[10];

            Console.WriteLine("Saisissez dix valeurs");
            
            for(int i=0;i<10;i++)
            {
                tab[i]= int.Parse(Console.ReadLine());
            }
            return(tab);
        }
    }
}
