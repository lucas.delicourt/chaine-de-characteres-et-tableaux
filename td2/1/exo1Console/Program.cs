﻿using System;

namespace exo1Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            string phrase;
           
            Console.Write("Saisissez une phrase : ");
            phrase = Console.ReadLine();
            
            Console.WriteLine("Vous avez saisi : {0}", phrase);

            Console.WriteLine(Program.debphrase(phrase));
            Console.WriteLine(Program.finphrase(phrase));

        }
        public static string debphrase(string phrase){
            char premiere_lettre = phrase[0];
            if(premiere_lettre>='A' && premiere_lettre<='Z')
            {
                return "Cette phrase commence par une majuscule !";
            }
            else
            {
                return "Cette phrase ne commence pas par une majuscule...";
            }
        }

        public static string finphrase(string phrase){
            char derniere_lettre = phrase[phrase.Length - 1];
            if(derniere_lettre=='.')
            {
                return("Cette phrase se termine par un point.");
            }
            else
            {
                return("Cette phrase ne se termine pas par un point.");
            }
        }
        
    }
}
