using System;
using Xunit;

using exo1Console;

namespace exo1test
{
    public class PhraseTest
    {
        [Fact]
        public void TestDebphraseAvecMaj()
        {
            Assert.NotEqual("Cette phrase ne commence pas par une majuscule !", Program.debphrase("Abc"));
            Assert.Equal("Cette phrase commence par une majuscule !", Program.debphrase("Abc"));
        }

        [Fact]
        public void TestDebphraseSansMaj()
        {
            Assert.NotEqual("Cette phrase commence par une majuscule !", Program.debphrase("abc"));
            Assert.Equal("Cette phrase ne commence pas par une majuscule...", Program.debphrase("abc"));
        }

        [Fact]
        public void TestFinphraseAvecPoint()
        {
            Assert.Equal("Cette phrase se termine par un point.", Program.finphrase("."));
            Assert.NotEqual("Cette phrase ne se termine pas par un point.", Program.finphrase("."));

        }

        [Fact]
        public void TestFinphraseSansPoint()
        {
            Assert.Equal("Cette phrase ne se termine pas par un point.", Program.finphrase(" "));
            Assert.NotEqual("Cette phrase se termine par un point.", Program.finphrase(" "));
        }
    }
}
